class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        total = 0
        for product in self.products:
            total = total + product.get_price()
        return total

    def get_total_quantity_of_products(self):
        total = 0
        for product in self.products:
            total += product.quantity
        return total

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.quantity * self.unit_price
